﻿using lojachinelosanderson.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DBestoque
{
    class EstoqueDatabase
    {

        public int Salvar(EstoqueDto dto)
        {
            string script = @"INSERT INTO tb_estoque (ds_quantidade,nm_acessorio)
                                  VALUES (@ds_quantidade, @nm_acessorio)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("nm_acessorio", dto.acessorio));
           


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<EstoqueDto> Listar()
        {
            string script = @"SELECT * FROM tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDto> lista = new List<EstoqueDto>();
            while (reader.Read())
            {
                EstoqueDto dto = new EstoqueDto();
                dto.Id = reader.GetInt32("id_estoque");
                dto.acessorio = reader.GetString("nm_acessorio");
                dto.quantidade = reader.GetInt32("ds_quantidade");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<EstoqueDto> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_estoque WHERE nm_acessorio like nm_acessorio";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_acessorio", produto + "%"));

          

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDto> lista = new List<EstoqueDto>();
            while (reader.Read())
            {
                EstoqueDto dto = new EstoqueDto();
                dto.Id = reader.GetInt32("id_estoque");
                dto.acessorio = reader.GetString("nm_acessorio");
                dto.quantidade = reader.GetInt32("ds_quantidade");
            

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public void Alterar(EstoqueDto dto)
        {
            string script = @"UPDATE tb_estoque 
                                SET ds_quantidade =@ds_quantidade + ds_quantidade 
                               WHERE nm_acessorio = 'Borracha'";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
           
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));
           

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Alterar1(EstoqueDto dto)
        {
            string script = @"UPDATE tb_estoque 
                                 SET ds_quantidade =@ds_quantidade + ds_quantidade 
                               WHERE nm_acessorio = 'Correia'";

            List<MySqlParameter> parms = new List<MySqlParameter>();
           
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Alterar2(EstoqueDto dto)
        {
            string script = @"UPDATE tb_estoque 
                                 SET ds_quantidade =@ds_quantidade - ds_quantidade 
                               WHERE nm_acessorio = 'Borracha'";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Alterar3(EstoqueDto dto)
        {
            string script = @"UPDATE tb_estoque 
                                 SET ds_quantidade =@ds_quantidade - ds_quantidade 
                               WHERE nm_acessorio = 'Correia'";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
    }
}

