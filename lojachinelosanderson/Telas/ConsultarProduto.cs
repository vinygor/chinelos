﻿using lojachinelosanderson.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class ConsultarProduto : Form
    {
        Validacao v = new Validacao();
        public ConsultarProduto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PersonalizadoBusiness business = new PersonalizadoBusiness();
                List<PersonalizadoDTO> lista = business.Consultar(txtProduto.Text);

                dgvPedidos.AutoGenerateColumns = false;
                dgvPedidos.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);

        }
    }
}
