﻿using lojachinelosanderson.DBcliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class ConsultarCliente : Form
    {
        Validacao v = new Validacao();
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cliente tela = new cliente();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

            Estoque tela = new Estoque();
            tela.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            Hide();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            try
            {
                clienteBusiness business = new clienteBusiness();
                List<clienteDTO> lista = business.Consultar(txtCliente.Text);

                dgvcliente.AutoGenerateColumns = false;
                dgvcliente.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void txtcliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
