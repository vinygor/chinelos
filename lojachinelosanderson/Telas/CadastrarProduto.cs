﻿using lojachinelosanderson.DB;
using lojachinelosanderson.Plug;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class CadastrarProduto : Form
    {
        Validacao v = new Validacao();

        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtborracha.Text == string.Empty)
                {
                    MessageBox.Show("Verifique se Todos os campos estão preenchidos corretamente");
                }
                else if (txtcorreia.Text == string.Empty)
                {
                    MessageBox.Show("Verifique se Todos os campos estão preenchidos corretamente");
                }
                else if (txtpreco.Text == string.Empty)
                {
                    MessageBox.Show("Verifique se Todos os campos estão preenchidos corretamente");
                }
                else if (txtproduto.Text == string.Empty)
                {
                    MessageBox.Show("Verifique se Todos os campos estão preenchidos corretamente");
                }
                else
                {
                    PersonalizadoDTO dto = new PersonalizadoDTO();
                    dto.Nome = txtproduto.Text;
                    dto.CorBorracha = txtborracha.Text;
                    dto.CorCorreia = txtcorreia.Text;
                    dto.Preço = Convert.ToDecimal(txtpreco.Text);
                    dto.Imagem = ImagemPlugin.ConverterParaString(picest.Image);

                    PersonalizadoBusiness business = new PersonalizadoBusiness();
                    business.Salvar(dto);

                    MessageBox.Show("Produto salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelos",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }


        }

        private void picest_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                picest.ImageLocation = dialog.FileName;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtpreco_TextChanged(object sender, EventArgs e)
        {
          
        }

        private void txtpreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.sonumeros(e);
        }

        private void txtcorreia_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcorreia_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtborracha_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }

        private void txtproduto_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }
    }
}
