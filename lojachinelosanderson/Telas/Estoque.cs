﻿using lojachinelosanderson.DBestoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class Estoque : Form
    {
        public Estoque()
        {
            InitializeComponent();
        }

        private void Estoque_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                List<EstoqueDto> lista = business.Listar();

                dgvproduto.AutoGenerateColumns = false;
                dgvproduto.DataSource = lista;


            }
            catch (Exception ex)
            {

                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                        "Chinelo",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
            }

            
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }

        private void dgvproduto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
