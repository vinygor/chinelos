﻿using lojachinelosanderson.DBestoque;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class Form3 : Form
    {
        BindingList<EstoqueDto> produtosCarrinho = new BindingList<EstoqueDto>();
        public Form3()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            EstoqueBusiness business = new EstoqueBusiness();
            List<EstoqueDto> lista = business.Listar();

            cboc.ValueMember = nameof(EstoqueDto.Id);
            cboc.DisplayMember = nameof(EstoqueDto.acessorio);
            cboc.DataSource = lista;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }
        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cliente tela = new cliente();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

            Estoque tela = new Estoque();
            tela.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                EstoqueDto nome = cboc.SelectedItem as EstoqueDto;

                EstoqueDto dto = new EstoqueDto();
                if (cboc.Text == "Borracha")
                {
                    dto.quantidade = dto.quantidade + Convert.ToInt32(nup1.Value);


                    EstoqueBusiness business = new EstoqueBusiness();
                    business.Alterar(dto);

                    MessageBox.Show("Produto salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else
                {
                    EstoqueDto nomes = cboc.SelectedItem as EstoqueDto;
                    dto.quantidade = dto.quantidade + Convert.ToInt32(nup1.Value);


                    EstoqueBusiness business = new EstoqueBusiness();
                    business.Alterar1(dto);

                    MessageBox.Show("Produto salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


                if (nup1.Value == 0)
                {
                    MessageBox.Show("Ocorreu um erro. Todos os campos devem estar preenchidos. ");


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }



        }

        private void txtpro_TextChanged(object sender, EventArgs e)
        {
            

        }

        private void cboc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}

