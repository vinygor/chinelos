﻿using lojachinelosanderson.DB;
using lojachinelosanderson.DBcliente;
using lojachinelosanderson.DBestoque;
using lojachinelosanderson.Plug;
using lojachinelosanderson.Vendas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class Form1 : Form
    {
        Validacao v = new Validacao();

        BindingList<PersonalizadoDTO> produtosCarrinho = new BindingList<PersonalizadoDTO>();
        public Form1()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarCombo();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            PersonalizadoBusiness business = new PersonalizadoBusiness();
            List<PersonalizadoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(PersonalizadoDTO.Id);
            cboProduto.DisplayMember = nameof(PersonalizadoDTO.Nome);
            cboProduto.DataSource = lista;
        }
        void CarregarCombo()
        {
            clienteBusiness busines = new clienteBusiness();
            List<clienteDTO> list = busines.Listar();

            cboc.ValueMember = nameof(clienteDTO.Id);
            cboc.DisplayMember = nameof(clienteDTO.Nome);
            cboc.DataSource = list;
        }
        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }
        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cliente tela = new cliente();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

            Estoque tela = new Estoque();
            tela.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            PersonalizadoDTO dto = cboProduto.SelectedItem as PersonalizadoDTO;
            imgfoto.Image = ImagemPlugin.ConverterParaImagem(dto.Imagem);
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            clienteDTO dto = cboc.SelectedItem as clienteDTO;
            txtcpf.Text = dto.CPF;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
               
                
                if (txt1.Text == string.Empty)
                {
                    MessageBox.Show("Verifique se todos os campos estão preenchidos corretamente");
                }
                else
                {
                    PersonalizadoDTO dto = cboProduto.SelectedItem as PersonalizadoDTO;

                    for (int i = 0; i < int.Parse(txt1.Text); i++)
                    {
                        produtosCarrinho.Add(dto);
                    }

                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void txtcpf_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            EstoqueDto dto = new EstoqueDto();

            try
            {


                dto.quantidade = dto.quantidade - Convert.ToInt32(txt1.Text);
                decimal i = dto.quantidade;
                
                
                    MessageBox.Show("Verifique a disponibilidade de correias no estoque");
                
                
                
                    EstoqueBusiness business = new EstoqueBusiness();
                    business.Alterar(dto);

                    EstoqueBusiness businesss = new EstoqueBusiness();
                    businesss.Alterar1(dto);











                    clienteDTO cliente = cboc.SelectedItem as clienteDTO;

                    vendaDTO dtos = new vendaDTO();

                    dtos.ClienteId = cliente.Id;


                    VendaBusiness businessss = new VendaBusiness();
                    businessss.Salvar(dtos, produtosCarrinho.ToList());

                    MessageBox.Show("Pedido salvo com sucesso.", "DBZ", MessageBoxButtons.OK, MessageBoxIcon.Information);






                
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }

}
       
    

	

