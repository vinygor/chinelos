﻿using lojachinelosanderson.DB;
using lojachinelosanderson.DBproduto;
using lojachinelosanderson.Vendas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class ConsultarVenda : Form
    {
        Validacao v = new Validacao();
        public ConsultarVenda()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {

                VendaBusiness business = new VendaBusiness();
                List<ConsultarView> lista = business.Consultar(txtCliente.Text);

                dgvp.AutoGenerateColumns = false;
                dgvp.DataSource = lista;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }


        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            TelaPrincipal tela = new TelaPrincipal();
            tela.Show();
            Hide();
        }

        private void txtvenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.soletras(e);
        }
    }
}
