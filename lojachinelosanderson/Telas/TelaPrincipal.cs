﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lojachinelosanderson.Telas
{
    public partial class TelaPrincipal : Form
    {
        public TelaPrincipal()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            cliente tela = new cliente();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
            Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {

            Estoque tela = new Estoque();
            tela.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarVenda tela = new ConsultarVenda();
            tela.Show();
            Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                Form1 tela = new Form1();
                tela.Show();
                Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro. Entre em contato com o administrador. " + ex.Message,
                    "Chinelo",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void telaPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void estoqueToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            cliente tela = new cliente();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {

            Estoque tela = new Estoque();
            tela.Show();
            Hide();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            Hide();
        }

        private void TelaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CadastrarProduto tela = new CadastrarProduto();
            tela.Show();
            Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ConsultarProduto tela = new ConsultarProduto();
            tela.Show();
            Hide();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
