﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DBcliente
{
    class clienteBusiness
    {
        public int Salvar(clienteDTO dto)
        {
            clienteDatabase db = new clienteDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(clienteDTO dto)
        {
            clienteDatabase db = new clienteDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            clienteDatabase db = new clienteDatabase();
            db.Remover(id);
        }

        public List<clienteDTO> Consultar(string cliente)
        {
            clienteDatabase db = new clienteDatabase();
            return db.Consultar(cliente);
        }

        public List<clienteDTO> Listar()
        {
            clienteDatabase db = new clienteDatabase();
            return db.Listar();
        }

    }



}

