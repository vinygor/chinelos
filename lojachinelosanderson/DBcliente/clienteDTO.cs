﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson
{
    class clienteDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string CEP { get; set; }
        public string bairro { get; set; }
        public string endereco { get; set; }
        public string cidade { get; set; }
        public string telefone { get; set; }
    }
}
