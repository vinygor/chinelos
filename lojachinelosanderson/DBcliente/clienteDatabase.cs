﻿using lojachinelosanderson.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DBcliente
{
    class clienteDatabase
    {
        public int Salvar(clienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_nome, ds_Cpf, ds_Cep, ds_bairro, ds_endereço, ds_cidade, ds_telefone ) 
                                  VALUES (@nm_nome, @ds_Cpf, @ds_Cep, @ds_bairro, @ds_endereço, @ds_cidade, @ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_Cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_Cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_endereço", dto.endereco));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public void Alterar(clienteDTO dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_Nome = @nm_nome,
                                     ds_Cpf   = @ds_Cpf,
                                     ds_Cep = @ds_Cep,
                                     ds_bairro = @ds_bairro,
                                     ds_endereço = @ds_endereço,
                                     ds_cidade = @ds_cidade,
                                     ds_telefone = @ds_telefone,
                               WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_Cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_Cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_bairro", dto.bairro));
            parms.Add(new MySqlParameter("ds_endereço", dto.endereco));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<clienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_nome like @nm_Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<clienteDTO> lista = new List<clienteDTO>();
            while (reader.Read())
            {
                clienteDTO dto = new clienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.CPF = reader.GetString("ds_Cpf");
                dto.CEP = reader.GetString("ds_Cep");
                dto.bairro = reader.GetString("ds_bairro");
                dto.endereco = reader.GetString("ds_endereço");
                dto.cidade = reader.GetString("ds_cidade");
                dto.telefone = reader.GetString("ds_telefone");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<clienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<clienteDTO> lista = new List<clienteDTO>();
            while (reader.Read())
            {
                clienteDTO dto = new clienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.CPF = reader.GetString("ds_Cpf");
                dto.CEP = reader.GetString("ds_Cep");
                dto.bairro = reader.GetString("ds_bairro");
                dto.endereco = reader.GetString("ds_endereço");
                dto.cidade = reader.GetString("ds_cidade");
                dto.telefone = reader.GetString("ds_telefone");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
