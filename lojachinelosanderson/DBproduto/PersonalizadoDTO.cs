﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DB
{
    class PersonalizadoDTO
    {
        public int Id { get; set; }
        public string Nome{ get; set; }
        public string Imagem { get; set; }
        public string CorBorracha { get; set; }
        public string CorCorreia{ get; set; }
        public decimal Preço { get; set; }
        
    }
}
