﻿using lojachinelosanderson.DBproduto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DB
{
    class PersonalizadoBusiness
    {
        public int Salvar(PersonalizadoDTO dto)
        {
            PersonalizadoDatabase db = new PersonalizadoDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(PersonalizadoDTO dto)
        {
            PersonalizadoDatabase db = new PersonalizadoDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            PersonalizadoDatabase db = new PersonalizadoDatabase();
            db.Remover(id);
        }

        public List<PersonalizadoDTO> Consultar(string produto)
        {
            PersonalizadoDatabase db = new PersonalizadoDatabase();
            return db.Consultar(produto);
        }

        public List<PersonalizadoDTO> Listar()
        {
            PersonalizadoDatabase db = new PersonalizadoDatabase();
            return db.Listar();
        }
       
    }
}
