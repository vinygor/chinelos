﻿using lojachinelosanderson.Base;
using lojachinelosanderson.DBproduto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.DB
{
    class PersonalizadoDatabase
    {
        public int Salvar(PersonalizadoDTO dto)
        {
            string script = @"INSERT INTO tb_produto (nm_nome, estampa, corborracha, ds_corcorreia, preco) VALUES (@nm_nome, @estampa, @corborracha, @ds_corcorreia, @preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("estampa", dto.Imagem));
            parms.Add(new MySqlParameter("corborracha", dto.CorBorracha));
            parms.Add(new MySqlParameter("ds_corcorreia", dto.CorCorreia));
            parms.Add(new MySqlParameter("preco", dto.Preço));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(PersonalizadoDTO dto)
        {
            string script = @"UPDATE tb_produto 
                                 SET nm_nome  = @nm_nome,
                                     estampa    = @estampa,
                                     corborracha = @corborracha,
                                    ds_corcorreia    = @ds_corcorreia,
                                     preco = @preco   
                               WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido ", dto.Id));
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("estampa", dto.Imagem));
            parms.Add(new MySqlParameter("corborracha", dto.CorBorracha));
            parms.Add(new MySqlParameter("ds_corcorreia", dto.CorCorreia));
            parms.Add(new MySqlParameter("preco", dto.Preço));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PersonalizadoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PersonalizadoDTO> lista = new List<PersonalizadoDTO>();
            while (reader.Read())
            {
                PersonalizadoDTO dto = new PersonalizadoDTO();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Nome = reader.GetString("nm_nome");
                dto.Imagem = reader.GetString("estampa");
                dto.CorBorracha = reader.GetString("corborracha");
                dto.CorCorreia = reader.GetString("ds_corcorreia");
                dto.Preço = reader.GetDecimal("preco");
                

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<PersonalizadoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PersonalizadoDTO> lista = new List<PersonalizadoDTO>();
            while (reader.Read())
            {
                PersonalizadoDTO dto = new PersonalizadoDTO();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Nome = reader.GetString("nm_nome");
                dto.Imagem = reader.GetString("estampa");
                dto.CorBorracha = reader.GetString("corborracha");
                dto.CorCorreia = reader.GetString("ds_corcorreia");
                dto.Preço = reader.GetDecimal("preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
       
    }
}

