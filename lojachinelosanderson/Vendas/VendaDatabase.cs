﻿using lojachinelosanderson.Base;
using lojachinelosanderson.DBproduto;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.Vendas
{
    class Vendadatabase
    {
         
        public int Salvar(vendaDTO dto)
        {
            string script = @"INSERT INTO tb_vendas (id_cliente) VALUES (@id_cliente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ClienteId));            
            



            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }





        public List<ConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM VW_PRODUTO_CONSULTA WHERE Nm_nome like @Nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ConsultarView> lista = new List<ConsultarView>();
            while (reader.Read())
            {
                ConsultarView dto = new ConsultarView();
                dto.Id = reader.GetInt32("id_vendas");
                dto.Cliente = reader.GetString("Nm_nome");
                dto.QtdItens = reader.GetInt32("qtd_itens");

                dto.Total = reader.GetDecimal("vl_total");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

       
    }
}
