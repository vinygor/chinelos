﻿using lojachinelosanderson.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lojachinelosanderson.Vendas
{
    class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO pedido_item (id_vendas, id_pedido) VALUES (@id_vendas, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_vendas", dto.IdVendas));
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM pedido_item WHERE id_pedido_item = @id_pedido_item";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_item", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PedidoItemDTO> ConsultarPorPedido(int idVendas)
        {
            string script = @"SELECT * FROM pedido_item WHERE id_vendas = @id_vendas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_vendas", idVendas));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemDTO> lista = new List<PedidoItemDTO>();
            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("id_pedido_item");
                dto.IdVendas = reader.GetInt32("id_vendas");
                dto.IdPedido = reader.GetInt32("id_pedido");
             

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
